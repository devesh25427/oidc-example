.PHONY: all build
CURDIR:=${PWD}
EXE=
ifeq ($(OS), Windows_NT)
	EXE=.exe
endif

#CLI
build:
	go build -o example$(EXE) main.go
