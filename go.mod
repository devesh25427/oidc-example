module devesh25427/oidc-example

go 1.15

require (
	github.com/coreos/go-oidc/v3 v3.0.0
	golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985
	golang.org/x/oauth2 v0.0.0-20210628180205-a41e5a781914
)
