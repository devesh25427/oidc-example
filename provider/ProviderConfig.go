package providerConfig

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)
var (
defaultConFile = "oidc.json"
)
/* read provider settings from Json */
type ProviderConfig struct {
	ProviderName string   `json:"name"`
	IssuerURL    string   `json:"issuer_url"`	//without /.well-known/openid-configuration
	ClientId     string   `json:"client_id"`
	ClientSecret string   `json:"client_secret"`
	Scopes       []string `json:"scopes"`
}


//New provider creates a provider by reading configuration from conf file
func NewProviderConfig(confFile *string) *ProviderConfig {
	if confFile == nil || *confFile == "" {
		fmt.Printf("Using default conf file %s \n", confFile)
		confFile = &defaultConFile
	}
	var p ProviderConfig
	//Read configuration from file
	if dat, err := ioutil.ReadFile(*confFile); err != nil{
		fmt.Errorf("Failed to open file Error : %s \n", err.Error())
		return nil
	}else{
		if err = json.Unmarshal(dat,&p); err != nil{
			fmt.Errorf("Failed to unmarshal Error : %s\n", err.Error())
			return nil
		}
	}
	conf,_ := json.MarshalIndent(&p, "", "    ")
	fmt.Printf("Configuration params are %s \n", string(conf))
	return &p
}
